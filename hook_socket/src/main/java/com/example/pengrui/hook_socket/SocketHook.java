package com.example.pengrui.hook_socket;

import java.net.SocketAddress;

import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

/**
 * Created by pengrui on 2016/3/11.
 */
public class SocketHook implements IXposedHookLoadPackage {
    String log_tag = "Task-ThreadOut";

    @Override
    public void handleLoadPackage(XC_LoadPackage.LoadPackageParam loadPackageParam) throws Throwable {
        String package_name = loadPackageParam.packageName;

        if(!package_name.equals("com.merry.color")) return;



        XposedHelpers.findAndHookMethod("java.net.Socket", loadPackageParam.classLoader, "connect", SocketAddress.class, int.class, new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                XposedBridge.log("After hook Socket Connect");

                Exception exception = new Exception("Socket connection");
                StackTraceElement[] elements = exception.getStackTrace();

                StringBuilder builder = new StringBuilder();
                for(StackTraceElement element : elements){
                    builder.append(element.toString() + "\r\n");
                }

                XposedBridge.log(builder.toString());

                super.afterHookedMethod(param);
            }
        });

        Class<?> arg_class = loadPackageParam.classLoader.loadClass("com.merry.color.n.a$a");

        XposedHelpers.findAndHookMethod("com.merry.color.n.a", loadPackageParam.classLoader, "a", arg_class, String.class, new XC_MethodHook() {
            @Override
            protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                XposedBridge.log("Before hook Send");
                super.beforeHookedMethod(param);
                String content = (String) param.args[1];
                XposedBridge.log(content);
            }
        });
    }
}
