package com.merry.modify;

import android.content.ContentResolver;
import android.content.pm.PackageInfo;
import android.os.Build;
import android.os.Build.VERSION;
import android.provider.Settings;
import android.util.DisplayMetrics;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.net.InetAddress;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XC_MethodReplacement;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage.LoadPackageParam;

public class ModifyPhoneBasicInfo implements IXposedHookLoadPackage{
//	String[] randomPackagesName = new String[]{"com.fake1","com.fake2","com.fake3","com.fake4","com.fake5","com.fake6","com.fake7",
//			"com.fake8","com.fake9","com.fake10"};

	private static final String targetPackage = "com.stephenchow.flashlight";
	
	@Override
	public void handleLoadPackage(final LoadPackageParam lpparam) throws Throwable {
		if (lpparam.packageName.startsWith("com.android") || lpparam.packageName.startsWith("de.robv.android.xposed")
				|| lpparam.packageName.startsWith("com.google") || lpparam.packageName.startsWith("com.stephenchow.merry.bartwork"))
			return;


		//clear SDcard
		if (lpparam.packageName.equals(targetPackage)) {
			XposedBridge.log("Ad App Start And need Clear Sdcard");
			File sdcardDir = new File("/storage/sdcard0/");
			File[] childFiles = sdcardDir.listFiles();
			for (int i = 0; i < childFiles.length; i++) {
				Util.delete(childFiles[i]);
			}
		}


		File configFile = new File("/sdcard/deviceConfig");
		if (!configFile.exists()) return;

		final DeviceConfig deviceConfig = new DeviceConfig(new JSONObject(Util.readFile(configFile)));
		hookBuild(deviceConfig);
		hookTelephony(lpparam, deviceConfig);
		hookWIFIInfo(lpparam, deviceConfig);
		hookSecure(lpparam, deviceConfig);
		hookPackages(lpparam);
		hookDisplay(lpparam, deviceConfig);
		if (lpparam.packageName.equals(targetPackage)) {
			hookNetWorkType(lpparam);

			XposedHelpers.findAndHookMethod("com.dgsdk.cp.f", lpparam.classLoader, "a", String.class, String.class, new XC_MethodHook() {
				@Override
				protected void afterHookedMethod(MethodHookParam param) throws Throwable {
					super.afterHookedMethod(param);

					XposedBridge.log("hook f.a " + param.args[0] + " " + param.args[1]);
				}
			});
		}



	}

	private void hookDisplay(LoadPackageParam lpparam, final DeviceConfig deviceConfig) {
		XposedHelpers.findAndHookMethod("android.view.Display", lpparam.classLoader, "getWidth", new XC_MethodReplacement() {

			@Override
			protected Object replaceHookedMethod(MethodHookParam param) throws Throwable {
				return deviceConfig.getWith();
			}
		});

		XposedHelpers.findAndHookMethod("android.view.Display", lpparam.classLoader, "getHeight", new XC_MethodReplacement() {

			@Override
			protected Object replaceHookedMethod(MethodHookParam param) throws Throwable {
				return deviceConfig.getHeight();
			}
		});

		XposedHelpers.findAndHookMethod("android.view.Display", lpparam.classLoader, "getMetrics", DisplayMetrics.class ,new XC_MethodHook() {

			@Override
			protected void afterHookedMethod(MethodHookParam param) throws Throwable {
				super.afterHookedMethod(param);

				DisplayMetrics arg = (DisplayMetrics) param.args[0];
				arg.densityDpi = deviceConfig.getDensity();
				arg.widthPixels = deviceConfig.getWith();
				arg.heightPixels = deviceConfig.getHeight();
			}
		});


		XposedHelpers.findAndHookMethod("android.content.res.Resources", lpparam.classLoader, "getDisplayMetrics", new XC_MethodHook() {

			@Override
			protected void afterHookedMethod(MethodHookParam param) throws Throwable {
				super.afterHookedMethod(param);

				DisplayMetrics dm = (DisplayMetrics) param.getResult();
				dm.densityDpi = deviceConfig.getDensity();
				dm.heightPixels = deviceConfig.getHeight();
				dm.widthPixels = deviceConfig.getWith();
			}
		});


	}

	private void hookPackages(LoadPackageParam lpparam) {
		if(lpparam.packageName.equals(targetPackage)){
			final File packageInfoFile = new File("/sdcard/packageInfo");
			if(packageInfoFile.exists()) {
				try {
					final JSONObject packageInfoJson = new JSONObject(Util.readFile(packageInfoFile));
					XposedHelpers.findAndHookMethod("android.app.ApplicationPackageManager", lpparam.classLoader, "getPackageInfo", String.class, int.class, new XC_MethodHook() {
						@Override
						protected void afterHookedMethod(MethodHookParam param) throws Throwable {
							super.afterHookedMethod(param);
							String packageName = (String) param.args[0];
							if (packageName.equals(targetPackage)) {
								PackageInfo result = (PackageInfo) param.getResult();
								result.packageName = packageInfoJson.getString("package_name");
								result.versionCode = packageInfoJson.getInt("version_code");
							}
						}
					});
				} catch (JSONException e) {
					e.printStackTrace();
					XposedBridge.log("hook getPackageInfo Exception" + e.toString());
				}
			}
		}



		XposedHelpers.findAndHookMethod("android.app.ApplicationPackageManager", lpparam.classLoader, "getInstalledPackages", int.class, new XC_MethodHook() {

			@Override
			protected void afterHookedMethod(MethodHookParam param) throws Throwable {
				super.afterHookedMethod(param);
				List<PackageInfo> packages = (List<PackageInfo>) param.getResult();
				Iterator<PackageInfo> iterator = packages.iterator();
				PackageInfo targetCloneInfo = null;
				while(iterator.hasNext()) {
					PackageInfo info = iterator.next();
					if (info.packageName.equals("de.robv.android.xposed.installer") || info.packageName.equals("com.example.android.livecubes") ||
							info.packageName.equals("org.proxydroid") || info.packageName.equals("com.genymotion.superuser") ||
							info.packageName.equals("jp.co.omronsoft.openwnn") || info.packageName.equals("com.merry.xpose.demo") ||
							info.packageName.equals("com.example.android.apis") || info.packageName.equals("com.android.gesture.builder")) {

						iterator.remove();
					}

					if(info.packageName.equals(targetPackage)){
						targetCloneInfo = info;
					}
				}



				String[] installPackages = Util.getRandomInstallPackages();
				XposedBridge.log("Invoke Get install Packages");
				for (int index = 0; index < installPackages.length; index++) {
					String randomPackageName = installPackages[index];
					PackageInfo cloneInfo = clone(targetCloneInfo);
					cloneInfo.packageName = randomPackageName;
					packages.add(cloneInfo);
				}
			}

			private PackageInfo clone(PackageInfo template) {
				PackageInfo ret = new PackageInfo();
				ret.activities = template.activities;
				ret.applicationInfo = template.applicationInfo;
				ret.configPreferences = template.configPreferences;
				ret.firstInstallTime = template.firstInstallTime;
				ret.gids = template.gids;
				ret.instrumentation = template.instrumentation;
				ret.lastUpdateTime = template.lastUpdateTime;
				ret.packageName = template.packageName;
				ret.permissions = template.permissions;
				ret.providers = template.providers;
				ret.receivers = template.receivers;
				ret.reqFeatures = template.reqFeatures;
				ret.requestedPermissions = template.requestedPermissions;
				if (VERSION.SDK_INT >= 16) {
					ret.requestedPermissionsFlags = template.requestedPermissionsFlags;
				}
				ret.services = template.services;
				ret.sharedUserId = template.sharedUserId;
				ret.sharedUserLabel = template.sharedUserLabel;
				ret.signatures = template.signatures;
				ret.versionCode = template.versionCode;
				ret.versionName = template.versionName;
				return ret;
			}

		});
	}

	private void hookSecure(LoadPackageParam lpparam, final DeviceConfig deviceConfig) {
		XposedHelpers.findAndHookMethod("android.provider.Settings.Secure", lpparam.classLoader, "getString", ContentResolver.class, String.class, new XC_MethodHook() {

			@Override
			protected void afterHookedMethod(MethodHookParam param) throws Throwable {
				super.afterHookedMethod(param);
				String name = (param.args.length > 1 ? (String) param.args[1] : null);
				if (Settings.Secure.ANDROID_ID.equals(name)) {
					param.setResult(deviceConfig.getAndroidID());
				}
			}

		});
	}

	private void hookNetWorkType(LoadPackageParam lpparam) {
		XposedHelpers.findAndHookMethod("android.net.NetworkInfo", lpparam.classLoader, "getType", new XC_MethodReplacement() {

            @Override
            protected Object replaceHookedMethod(MethodHookParam param) throws Throwable {
                return new Random().nextInt(2);
            }
        });


		XposedHelpers.findAndHookMethod("android.net.NetworkInfo", lpparam.classLoader, "getSubtype", new XC_MethodReplacement() {

            @Override
            protected Object replaceHookedMethod(MethodHookParam param) throws Throwable {
                int random = new Random().nextInt(3);
                switch (random) {
                    case 0:
                        return 1;
                    case 1:
                        return 2;
                    case 2:
                        return 4;
                    default:
                        return 1;
                }
            }

        });

		XposedHelpers.findAndHookMethod("android.net.NetworkInfo", lpparam.classLoader, "isConnected", new XC_MethodReplacement() {
            @Override
            protected Object replaceHookedMethod(MethodHookParam param) throws Throwable {
                return true;
            }
        });
	}

	private void hookWIFIInfo(LoadPackageParam lpparam, final DeviceConfig deviceConfig) {
		XposedHelpers.findAndHookMethod("java.net.InetAddress", lpparam.classLoader, "getHostAddress", new XC_MethodHook() {

			@Override
			protected void afterHookedMethod(MethodHookParam param) throws Throwable {
				super.afterHookedMethod(param);
				InetAddress address = (InetAddress) param.thisObject;
				if (address.isLinkLocalAddress()) {
					param.setResult(Util.generateIpv6LinkLocalAddress());
				}
			}
		});


		XposedHelpers.findAndHookMethod("android.net.wifi.WifiInfo", lpparam.classLoader, "getMacAddress", new XC_MethodReplacement() {

			@Override
			protected Object replaceHookedMethod(MethodHookParam param) throws Throwable {
				return deviceConfig.getMacAddr();
			}
		});

		XposedHelpers.findAndHookMethod("android.net.wifi.WifiInfo", lpparam.classLoader, "getIpAddress", new XC_MethodReplacement() {

			@Override
			protected Object replaceHookedMethod(MethodHookParam param) throws Throwable {
				return Util.generateLocalIP();
			}
		});

		XposedHelpers.findAndHookMethod("android.net.wifi.WifiInfo", lpparam.classLoader, "getBSSID", new XC_MethodReplacement() {

			@Override
			protected Object replaceHookedMethod(MethodHookParam param) throws Throwable {
				return Util.generateMACAddr(true);
			}
		});


		XposedHelpers.findAndHookMethod("android.net.wifi.WifiInfo", lpparam.classLoader, "getSSID", new XC_MethodReplacement() {

			@Override
			protected Object replaceHookedMethod(MethodHookParam param) throws Throwable {
				return Util.generateSSID();
			}
		});
	}

	private void hookTelephony(LoadPackageParam lpparam, final DeviceConfig deviceConfig) {
		XposedHelpers.findAndHookMethod("android.telephony.TelephonyManager", lpparam.classLoader, "getDeviceId", new XC_MethodReplacement() {

			@Override
			protected Object replaceHookedMethod(MethodHookParam param) throws Throwable {
				return deviceConfig.getDeviceID();
			}
		});


		XposedHelpers.findAndHookMethod("android.telephony.TelephonyManager", lpparam.classLoader, "getSubscriberId", new XC_MethodReplacement() {

			@Override
			protected Object replaceHookedMethod(MethodHookParam param) throws Throwable {
				return deviceConfig.getSubscribedID();
			}

		});


		XposedHelpers.findAndHookMethod("android.telephony.TelephonyManager", lpparam.classLoader, "getSimState", new XC_MethodReplacement() {

			@Override
			protected Object replaceHookedMethod(MethodHookParam param) throws Throwable {
				return 5;
			}
		});

		XposedHelpers.findAndHookMethod("android.telephony.TelephonyManager", lpparam.classLoader, "getSimSerialNumber", new XC_MethodReplacement() {

			@Override
			protected Object replaceHookedMethod(MethodHookParam param) throws Throwable {
				return generateSerialNumber();
			}

		});

		XposedHelpers.findAndHookMethod("android.telephony.TelephonyManager", lpparam.classLoader, "getNetworkOperator", new XC_MethodReplacement() {

			@Override
			protected Object replaceHookedMethod(MethodHookParam param) throws Throwable {
				return deviceConfig.getSubscribedID().substring(0, 5);
			}
		});

		XposedHelpers.findAndHookMethod("android.telephony.TelephonyManager", lpparam.classLoader, "getNetworkCountryIso", new XC_MethodReplacement() {

			@Override
			protected Object replaceHookedMethod(MethodHookParam param) throws Throwable {
				return "cn";
			}
		});

		XposedHelpers.findAndHookMethod("android.telephony.TelephonyManager", lpparam.classLoader, "getNetworkOperatorName", new XC_MethodReplacement() {

			@Override
			protected Object replaceHookedMethod(MethodHookParam param) throws Throwable {
				 char bit = deviceConfig.getSubscribedID().charAt(4);
				 switch (bit){
					 case '0':
					 case '2':
					 case '7':
						 return "中国移动";

					 case '1':
					 case '6':
						 return "中国联通";

					 case '3':
					 case '5':
						 return "中国电信";

					 default:
						 return "中国移动";
				 }
			}
		});
	}


	public static String generateSerialNumber() {
		Random rand = new Random();
	    String serial = "";
	    for (int i = 0; i < 14; i++)
	        serial += Integer.toString(rand.nextInt(10));
	    return serial;
	}


	private void hookBuild(DeviceConfig deviceConfig){
		ChangeFinal.setFinalStatic(Build.class, "ID", deviceConfig.get_ID());
		ChangeFinal.setFinalStatic(Build.class, "BOARD", deviceConfig.getBoard());
		ChangeFinal.setFinalStatic(Build.class, "BOOTLOADER", deviceConfig.getBootloader());
		ChangeFinal.setFinalStatic(Build.class, "BRAND", deviceConfig.getBrand());
		ChangeFinal.setFinalStatic(Build.class, "DEVICE", deviceConfig.getDevice());
		ChangeFinal.setFinalStatic(Build.class, "MODEL", deviceConfig.getModel());
		ChangeFinal.setFinalStatic(Build.class, "FINGERPRINT", deviceConfig.getFingerprint());
		ChangeFinal.setFinalStatic(Build.class, "MANUFACTURER", deviceConfig.getManufacture());
		ChangeFinal.setFinalStatic(Build.class, "PRODUCT", deviceConfig.getProduct());
		ChangeFinal.setFinalStatic(Build.class, "SERIAL", deviceConfig.getSerial());
		ChangeFinal.setFinalStatic(Build.class, "DISPLAY", deviceConfig.getDisplay());
		ChangeFinal.setFinalStatic(Build.VERSION.class, "RELEASE" , deviceConfig.getRelease());
		ChangeFinal.setFinalStatic(Build.VERSION.class, "SDK", String.valueOf(deviceConfig.getSdk()));
		ChangeFinal.setFinalStatic(Build.VERSION.class, "SDK_INT", deviceConfig.getSdk());
	}
}
