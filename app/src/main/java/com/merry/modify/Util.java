package com.merry.modify;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Map.Entry;
import java.util.zip.GZIPInputStream;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.transform.Templates;

import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.params.ConnRouteParams;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;

import android.R.integer;


public class Util {
	public static byte[] encrypt(byte[] data) {
		byte[] keyBytes = new byte[16];

		for (int index = 0; index < 16; index++) {
			keyBytes[index] = (byte) (32 + index);
		}

		try {
			SecretKeySpec key = new SecretKeySpec(keyBytes, "AES");
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, key);
			byte[] result = cipher.doFinal(data);
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static byte[] decrypt(byte[] data){
		byte[] keyBytes = new byte[16];

		for (int index = 0; index < 16; index++) {
			keyBytes[index] = (byte) (32 + index);
		}
		
		try {
			SecretKeySpec key = new SecretKeySpec(keyBytes, "AES");
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			cipher.init(Cipher.DECRYPT_MODE, key);
			byte[] result = cipher.doFinal(data);
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static String readFile(File file){
		try {
			return new String(readFileRaw(file), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static byte[] readFileRaw(File file){
		try {
			InputStream input = new FileInputStream(file);
			ByteArrayOutputStream output = new ByteArrayOutputStream();
			byte[] buffer = new byte[1024];
			int read = 0;
			while((read = input.read(buffer)) != -1){
				output.write(buffer, 0, read);
			}
			
			input.close();
			output.close();
			
			return output.toByteArray();
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}	
	}
	
	public static void writeFile(File file, String content){
		try {
			OutputStream output = new FileOutputStream(file,true);
			output.write(content.getBytes());
			output.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static void writeFileRaw(File file, byte[] data){
		try {
			OutputStream output = new FileOutputStream(file);
			output.write(data);
			output.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static String generateMACAddr(boolean hasSem) {
		String ret = null;
		Random rand = new Random();
		String mac = "";

	    for (int i = 0; i < 12; i++)
	    {
	    	if(i != 0 && hasSem && i%2==0 ) mac += ":";
	    	mac += Integer.toString(rand.nextInt(16), 16);
	    }
	    
		ret = mac;
		
	    return ret;
	   
	}
	
	
	static String[] templates = new String[]{"TPLINK","360WIFI","HUAWEI","XIAOMI","Domino","TENDA","ChinaNet","Unicom"};
	public static String generateSSID(){
		StringBuilder randomTempate = new StringBuilder(templates[new Random().nextInt(templates.length)]);
		randomTempate.append("-");
		int randomLength = 3 + new Random().nextInt(2);
		for(int index = 0; index < randomLength; index++ ){
			randomTempate.append(Integer.toHexString(new Random().nextInt(64)).toUpperCase());
		}
		
		return randomTempate.toString();
	}
	
	public static int generateLocalIP(){
		int base = 0;
		if(new Random().nextBoolean()){
			base = 192|168<<8;
		}else{
			base = 10|80<<8;
		}
		
		base |= new Random().nextInt(255) << 16;
		base |= new Random().nextInt(255) << 24;
		
		return base;
	}
	
	static String[] installTemplate = new String[]{
		"com.qihoo.antivirus","com.qihoo.appstore","com.qihoo.lottery",
		"com.qihoo360.mobilesafe","com.qihoo360.mobilesafe.opti","com.qihu.tools",
		"com.tools.androidsystemcleaner","com.kadbbz.smartcleaner",
		"com.lenovo.safecenter.lite.boot","cn.com.opda.android.clearmaster",
		"cn.com.opda.android.optimizebox.pad","com.tencent.mm",
		"com.tencent.mobileqq","com.tencent.mtt","com.tencent.qqpimsecure",
		"com.baidu.BaiduMap","com.baidu.browser.apps","com.baidu.searchbox",
		"com.taobao.taobao","com.eg.android.AlipayGphone","cn.amazon.mShop.android",
		"cn.cntvnews","cn.com.fetion","com.android.bankabc",
		"com.autonavi.minimap","com.chinamworld.main","com.icbc",
		"com.netease.newsreader.activity","com.sinovatech.unicom.ui",
		"com.suning.mobile.ebuy","cn.goapk.market",
		"com.yingyonghui.market","com.wandoujia.phoenix2",
		"com.sogou.appmall","com.tencent.android.qqdownloader",
		"com.hiapk.marketpho","com.mappn.gfan",
		"com.dragon.android.pandaspace","com.baidu.appsearch",
		"com.sankuai.meituan.takeoutnew","com.sankuai.movie","com.sankuai.meituan","com.sankuai.meituan.takeoutnew",
		"com.letv.android.client","com.youku.phone","com.immomo.momo","jp.naver.line.android",
		"com.alibaba.mobileim","me.imid.fuubo","com.snda.wifilocating","com.sina.weibo","com.douban.book.reader"};
	public static String[] getRandomInstallPackages(){
		int pacakgesNum = new Random().nextInt(10) + 18;
		String[] ret = new String[pacakgesNum];
		
		for(int index = 0; index < pacakgesNum; index++){
			ret[index] = installTemplate[new Random().nextInt(installTemplate.length)];
		}
		
		return ret;
	}
	
	
	public static String generateIpv6LinkLocalAddress(){
		StringBuilder prefix =new StringBuilder("fe80::");
		for(int index = 1; index < 5; index++){
			prefix.append(Integer.toHexString(new Random(System.currentTimeMillis()*index).nextInt(16)));
			prefix.append(Integer.toHexString(new Random(System.currentTimeMillis()*2*index).nextInt(16)));
			prefix.append(Integer.toHexString(new Random(System.currentTimeMillis()*3*index).nextInt(16)));
			prefix.append(Integer.toHexString(new Random(System.currentTimeMillis()*4*index).nextInt(16)));
			prefix.append(":");
		}
		
		String ret = prefix.substring(0, prefix.length() - 1);
		return ret +"%"+ (new Random().nextBoolean()?"wlan0":"eth1");
	}
	
	
	public static String exec(String command){
		try {
			Process process = Runtime.getRuntime().exec(command);
			BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream(), "UTF-8"));
			StringBuilder builder = new StringBuilder();
			String line = null;
			while((line = reader.readLine()) != null){
				builder.append(line).append("\n");
			}
			process.destroy();
			return builder.toString();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void delete(File file) {
		if (file.isFile()) {
			if(!file.getName().equals("deviceConfig")
					&& !file.getName().equals("packageInfo")){
				file.delete();
			}
			return;
		}

		if (file.isDirectory()) {
			if (!file.getName().equals("qumi") && !file.getName().equals("bart_world")) {
				File[] childFiles = file.listFiles();
				if (childFiles == null || childFiles.length == 0) {
					file.delete();
					return;
				}

				for (int i = 0; i < childFiles.length; i++) {
					delete(childFiles[i]);
				}
				file.delete();
			}
		}
	}
}


