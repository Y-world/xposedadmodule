package com.merry.modify;

import java.util.Locale;
import java.util.jar.Attributes.Name;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DeviceConfig {
	String serial;

	String fingerprint;
	String product;
	String bootloader;
	String device;
	String model;
	String manufacture;
	String board;
	String brand;
	String release;
	String local;
	String display;



	String ID;
	int with;
	int height;
	int density;
	int sdk;
	
	String deviceID;
	String subscribedID;
	String macAddr;
	String androidID;
	
	String[] randomInstallPacakges;

	public DeviceConfig(JSONObject deviceConfig) throws JSONException {
		this(deviceConfig.getString("buildID"), deviceConfig.getString("locale"), deviceConfig.getString("serial"), deviceConfig.getString("fingerprint"),
				deviceConfig.getString("product"), deviceConfig.getString("bootloader"), deviceConfig
						.getString("device"), deviceConfig.getString("model"), deviceConfig.getString("manufacturer"),
				deviceConfig.getString("board"), deviceConfig.getString("brand"), deviceConfig.getString("release"),
				deviceConfig.getInt("width_pixels"), deviceConfig.getInt("height_pixels"), deviceConfig.getInt("density_dpi"),
				deviceConfig.getInt("sdk_ver"), deviceConfig.getString("imei"), deviceConfig.getString("imsi"),
				deviceConfig.getString("mac_address"), deviceConfig.getString("android_Id"), deviceConfig.getString("display")/*,toStringArray(deviceConfig.getJSONArray("ExtraPackages"))*/);
	}
	
	private static String[] toStringArray(JSONArray array){
		String[] ret = new String[array.length()];
		for(int index = 0; index < array.length(); index++){
			try {
				ret[index] = array.getString(index);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		
		return ret;
	}

	private DeviceConfig(String id, String local,String serial, String fingerprint, String product, String bootloader,
			String device, String model, String manufacture, String board, String brand, String release, int with,
			int height, int density, int sdk, String deviceID, String subscribedID, String macAddr, String androidID, String display/*,String[] extraPackages*/) {
		super();
		this.ID = id;
		this.local = local;
		this.serial = serial;
		this.fingerprint = fingerprint;
		this.product = product;
		this.bootloader = bootloader;
		this.device = device;
		this.model = model;
		this.manufacture = manufacture;
		this.board = board;
		this.brand = brand;
		this.release = release;
		this.with = with;
		this.height = height;
		this.density = density;
		this.sdk = sdk;
		this.deviceID = deviceID;
		this.subscribedID = subscribedID;
		this.macAddr = macAddr;
		this.androidID = androidID;
		this.display = display;
		
		//this.randomInstallPacakges = extraPackages;
	}

	
	public Locale getLcoale(){
		String[] lanAndCountry = this.local.split("_");
		return new Locale(lanAndCountry[0], lanAndCountry[1]);
	}

	public String getMacAddr(){
		return macAddr;
	}
	
	public String getSerial() {
		return serial;
	}

	public String getFingerprint() {
		return fingerprint;
	}

	public String getProduct() {
		return product;
	}

	public String getBootloader() {
		return bootloader;
	}

	public String getDevice() {
		return device;
	}

	public String getModel() {
		return model;
	}

	public String getManufacture() {
		return manufacture;
	}

	public String getBoard() {
		return board;
	}

	public String getBrand() {
		return brand;
	}

	public String getRelease() {
		return release;
	}

	public int getWith() {
		return with;
	}

	public int getHeight() {
		return height;
	}

	public int getDensity() {
		return density;
	}

	public int getSdk() {
		return sdk;
	}
	
	public String getDeviceID(){
		return deviceID;
	}
	
	public String getSubscribedID(){
		return subscribedID;
	}
	
	public String getAndroidID(){
		return androidID;
	}

	
	public String[] getInstalledPacakges(){
		return randomInstallPacakges;
	}

	public String getDisplay() {
		return display;
	}

	public String get_ID() {
		return ID;
	}
}
