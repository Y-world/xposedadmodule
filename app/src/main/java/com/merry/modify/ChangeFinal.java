package com.merry.modify;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;


public class ChangeFinal {

	static void setFinalStatic(Field field, Object newValue) throws Exception {
		if(field==null)return;
		field.setAccessible(true);  
		 try { 
        	 Field modifiersField = Field.class.getDeclaredField("modifiers");  
             modifiersField.setAccessible(true);  
             modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL); 
            
		} catch (Throwable e) {
			// TODO: handle exception
		
        }
		 field.set(null, newValue); 
    }  
	
	static void setFinalStatic(Class clazz, String field, Object newValue){
		try {
			setFinalStatic(getField(clazz, field), newValue);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static Field getField(Class<?> xx,String field){
		try {
			return xx.getDeclaredField(field);
		} catch (Throwable e) {
			e.printStackTrace();
			return null;
		}
	}
}